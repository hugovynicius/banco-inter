# Teste/Desafio Banco Inter

Desafio/teste implementado por Hugo Bitencourt como parte do processo seletivo do Banco Inter.

### Pré-requisitos

É necessário a instalação dos seguintes softwares: Spring Tools 4 (recomendo a utilização dessa IDE), Maven 3.6, Java 8/superior, banco de dados H2. O Projeto foi desenvolvido e testado utilizando o Sistema Operacional Windows 10. 

## Compilar e Executar da aplicação

A compilação e execução da apicação pode ser feita de duas formas: na IDE Spring Tools 4 ou utilizando o Maven por meio de linha de comando. Os endereços dos endpoits apresentados abaixo são referentes ao computador do desenvolvedor. 

Na IDE, execute a Classe APP.java como Spring Boot APP. Siga os seguintes passos: botão direito na classe/Run As/Spring Boot APP

Abra o prompt de comando e acesse a pasta do projeto. Digite o comando abaixo para compilar e subir a aplicação. 
```
mvn spring:boot:run
```
Teste se aplicação iniciou corretamente acessando no Browser o link abaixo, e a resposta esperada deve ser "APP OK" 

```
http://localhost:8090/api/usuario/inter/
```
Além disso, é possivel compilar a aplicação dentro da IDE utilizando o Maven. 

## Execução dos testes

### Testes em geral 

Usando o Postman ou um aplicativo semelhante, teste as funcionalidades da aplicação utilizando as requisições abaixo: 

#### Inserir um usuário (endereço e corpo da mensagem):
```
http://localhost:8090/api/usuario/salvar/
{
	"nome": "Hugo",
	"email": "hugo@teste.com"
}
```

#### Calcular o dígito único (endereço e corpo da mensagem):   
```
http://localhost:8090/api/usuario/calcula/
{
	"n": "9875",
	"k":"4"
}
```
#### Listar os dígitos único de um determinado usuário (paramêtro id usuário = 1):
```
http://localhost:8090/api/usuario/digito/1
```

#### Listar de todos os usuários:
```
http://localhost:8090/api/usuario/
```

#### Retornar um usuário determinado (parâmetro id usuário = 1):
```
http://localhost:8090/api/usuario/1
```

#### Buscar a chave pública para criptografia:
```
http://localhost:8090/api/usuario/chave/
```

Além disso, na raiz do projeto contém um arquivo chamado postman_collection.json. Esse arquivo contém todos os testes descritos acima em forma de uma collection no Postman com testes automátizados.

### Testes unitários 

Foram implementados cinco testes unitários utilizando o Junit. Abaixo é apresentada uma breve descrição dos testes unitários.

* TestAplicacao.java: testa a inserção dos usuários; a listagem de todos os usuários; pesquisa de um usuário determinado; busca pelos digitos únicos de um usuário; testa o cache. 
* TesteCache.java: Testa o Cache.
* TesteCriptografia.java: testa a encriptação e decriptação do algoritmo de criptografia desenvolvido
* TesteDigitoUnico.java: testa o cálculo do dígito único  
* TesteP.java: Testa o cálculo do P.

Os testes unitários podem ser executados de duas formas: na IDE Spring Tools 4 ou utilizando o Maven por meio de linha de comando. 

Na IDE, execute as classes citadas acima como JUnit Test. Siga os seguintes passos: botão direito na classe/Run As/JUnit Test

Abra o prompt de comando e acesse a pasta do projeto. Digite os comandos abaixo para executar todos, um, dois testes unitários. 

```
mvn test
mvn -Dtest=TesteAplicacao test
mvn -Dtest=TesteAplicacao,TestDigitoUnico test
```

Além disso, é possivel executar os testes unitários dentro da IDE utilizando o Maven. 

### Acesso ao banco de dados H2

```
http://localhost:8090/h2
```
O login e senha estão disponíveis no arquivo application.propeties

## Autor

* **Hugo Bitencourt** 

## Agradecimentos

* Banco Inter