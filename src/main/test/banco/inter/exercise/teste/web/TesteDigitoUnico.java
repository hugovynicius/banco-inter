package banco.inter.exercise.teste.web;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

import banco.inter.exercise.calc.DigitoUnico;

public class TesteDigitoUnico {

	private DigitoUnico digito = new DigitoUnico("9875",4);
	
	@Test
	public void testP() {		
		System.out.println("Parâmetros: " );
		System.out.println("N: " + digito.getParametros().getN());
		System.out.println("K: " + digito.getParametros().getK());
		System.out.println("Resultado: " + digito.getResultado());
		
		Integer resultadoEsperado = 8;
		
		assertEquals(resultadoEsperado.intValue(), digito.getResultado().intValue());
	}
}
