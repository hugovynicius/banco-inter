package banco.inter.exercise.teste.web;

import org.junit.Test;

import banco.inter.exercise.cache.Cache;
import banco.inter.exercise.calc.DigitoUnico;

public class TesteCache {
	
	private static int MIN = 1;
	private static int MAX = 10000;
	private static int MAX_5 = 4;
	
	
	@Test
	public void testeCache() {
		
		Cache.getInstance();
		
		System.out.println("....Teste com 10 elemetos...");
		for (int i = 0; i < 10; i++) {
			Integer n = (int) ((Math.random() * ((MAX - MIN) + 1)) + MIN);
			Integer k = (int) ((Math.random() * ((MAX_5 - MIN) + 1)) + MIN);
			DigitoUnico digito = new DigitoUnico(String.valueOf(n), k);
			Cache.put(digito.getParametros().getN().toString() + digito.getParametros().getK().toString(),
					digito.getResultado());
		}
		
		System.out.println("Tamanho do cache = " + Cache.size());
		System.out.println("listando elementos: ");
		Cache.list();
		
		
		System.out.println("Teste com 5 elemetos");
		for (int i = 0; i < 6; i++) {
			Integer n = (int) ((Math.random() * ((MAX - MIN) + 1)) + MIN);
			Integer k = (int) ((Math.random() * ((MAX_5 - MIN) + 1)) + MIN);
			DigitoUnico digito = new DigitoUnico(String.valueOf(n), k);
			Cache.put(digito.getParametros().getN().toString() + digito.getParametros().getK().toString(),
					digito.getResultado());
		}
		System.out.println("Tamanho do cache = " + Cache.size());
		System.out.println("listando elementos: ");
		Cache.list();
			
	}
}
