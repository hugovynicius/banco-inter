package banco.inter.exercise.teste.web;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.security.PrivateKey;
import java.security.PublicKey;

import org.junit.Test;

import banco.inter.exercise.criptografia.CriptografiaRSA;

public class TesteCriptografia {
	
	private CriptografiaRSA criptografia = new CriptografiaRSA();

	@SuppressWarnings("resource")
	@Test
	public void testeCriptografia() throws Exception {
		
		try {

			// Verifica se já existe um par de chaves, caso contrário gera-se as chaves..
			if (!criptografia.verificaSeExisteChavesNoSO()) {
				criptografia.geraChave();
			}
			
			for(int i= 0; i < 3;i++) {
				final String msgOriginal = "Hugo";
				ObjectInputStream inputStream = null;

				// Criptografa a Mensagem usando a Chave Pública
				inputStream = new ObjectInputStream(new FileInputStream(CriptografiaRSA.PATH_CHAVE_PUBLICA));
				final PublicKey chavePublica = (PublicKey) inputStream.readObject();
				final byte[] textoCriptografado = criptografia.criptografa(msgOriginal, chavePublica);

				// Decriptografa a Mensagem usando a Chave Pirvada
				inputStream = new ObjectInputStream(new FileInputStream(CriptografiaRSA.PATH_CHAVE_PRIVADA));
				final PrivateKey chavePrivada = (PrivateKey) inputStream.readObject();
				final String textoPuro = criptografia.decriptografa(textoCriptografado, chavePrivada);

				// Imprime o texto original, o texto criptografado e
				// o texto descriptografado.
				System.out.println("Mensagem Original: " + msgOriginal);
				System.out.println("Mensagem Criptografada: " + textoCriptografado.toString());
				System.out.println("Mensagem Decriptografada: " + textoPuro);
				
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
