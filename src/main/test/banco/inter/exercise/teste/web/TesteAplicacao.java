package banco.inter.exercise.teste.web;



import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import banco.inter.exercise.App;
import banco.inter.exercise.cache.Cache;
import banco.inter.exercise.dto.FormDigito;
import banco.inter.exercise.dto.FormUsuario;
import banco.inter.exercise.service.UsuarioService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
public class TesteAplicacao {

	@Autowired
	private UsuarioService usuarioService;
	
	
	@Before
	public void testeInsereUsuario() {
		System.out.println("....Início: testeInsereUsuario()....");
		for(int i = 0; i < 5; i++) {
			String nome = "Nome " + String.valueOf(i);
			String email = "email " + String.valueOf(i);
			FormUsuario form = new FormUsuario();
			form.setNome(nome);
			form.setEmail(email);
			String usr = usuarioService.save(form);
			if(usr != null && usr.compareToIgnoreCase("Sucesso ao salvar usuário") == 0) {
				System.out.println("Usuário inserido com sucesso!");
			}
		}
		
		System.out.println("....Fim: testeInsereUsuario()....");
	}
	
	@Test
	public void testListaUsuarios() {
		System.out.println("....Início: testListaUsuarios()....");
		System.out.println("....Lista de usuários descriptografados....");
		List<FormUsuario> listUsuarios = usuarioService.getAll();
		listUsuarios.forEach(f -> {
			System.out.println("ID usuario: " + f.getNome());
			System.out.println("Nome usuario: " + f.getNome());
			System.out.println("Email usuario: " + f.getEmail());
			f.getDigitos().forEach(d -> System.out.println("Digito:" + d.getResultado()));
			System.out.println("");
		});
		System.out.println("....Fim: testListaUsuarios()....");
	}
	
	@Test 
	public void testListaDigitosUsuario() {
		System.out.println("....Início: testListaDigitosUsuario()....");
		Long idUsr = 2L;
		List<FormDigito> lista = usuarioService.getDigitosUsuario(idUsr);
		for(FormDigito d : lista) {
			System.out.println("Dígito: " + d.getResultado());
		}
		System.out.println("....Início: testListaDigitosUsuario()....");
		
	}
	
	@After
	public void testCache() {
		System.out.println("....Início: testCache()....");
		System.out.println("Tamanho cache = " + Cache.size());
		System.out.println("Listando elementos do cache: ");
		Cache.list();
		Cache.clear();
		System.out.println("....Fim: testCache()....");
	}
		
	
}
