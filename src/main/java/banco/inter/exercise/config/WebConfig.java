package banco.inter.exercise.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
//@ComponentScan({"banco.inter.exercise", "banco.inter.model"})
@EnableWebMvc
public class WebConfig {

}
