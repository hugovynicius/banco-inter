package banco.inter.exercise.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Table(name="usuario")
public class Usuario implements Serializable{
    
	private static final long serialVersionUID = 2194096690826900927L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty(message = "Equipment must have name.")
    @Column(name="nome", length = 2048)
    private byte[] nome;
    //private String nome;
   
    @Column(name="email", length = 2048)
    private byte[] email;
    //private String email;
    
    //Lista de dígitos únicos gerados para o usuário
  	@OneToMany(mappedBy="usuario")
  	@JsonIgnore
    private List<Digito> digitos;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public byte[] getNome() {
		return nome;
	}

	public void setNome(byte[] nome) {
		this.nome = nome;
	}

	public byte[] getEmail() {
		return email;
	}

	public void setEmail(byte[] email) {
		this.email = email;
	}

	public List<Digito> getDigitos() {
		return digitos;
	}

	public void setDigitos(List<Digito> digitos) {
		this.digitos = digitos;
	}   
  
}
