package banco.inter.exercise.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="digito")
public class Digito implements Serializable{

	private static final long serialVersionUID = 1L;

	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	 private Long id;
	 
	 @Column(name="paramentro_n") 
	 private Integer parametroN;
	 
	 @Column(name="paramentro_k")
	 private Integer parametroK;
	 
	 @Column(name="resultado")
	 private Integer resultado;
	 
	 @ManyToOne(targetEntity = Usuario.class, optional = false)
	 @JoinColumn(name="id_usuario", nullable = false, foreignKey = @ForeignKey(name="fk_usuario_digito"))
	 private Usuario usuario;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getParametroN() {
		return parametroN;
	}

	public void setParametroN(Integer parametroN) {
		this.parametroN = parametroN;
	}

	public Integer getParametroK() {
		return parametroK;
	}

	public void setParametroK(Integer parametroK) {
		this.parametroK = parametroK;
	}

	public Integer getResultado() {
		return resultado;
	}

	public void setResultado(Integer resultado) {
		this.resultado = resultado;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	 
	 
}
