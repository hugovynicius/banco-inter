package banco.inter.exercise.dto;

import java.util.List;

import banco.inter.exercise.model.Digito;

public class FormUsuario {
	private Long id;
	private String nome;
	private String email;
	private List<FormDigito> digitos;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<FormDigito> getDigitos() {
		return digitos;
	}
	public void setDigitos(List<FormDigito> digitos) {
		this.digitos = digitos;
	}
		
}
