package banco.inter.exercise.cache;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.stereotype.Component;

@Component 
public class Cache{

	private static SimpleCacheManager instance;
	private static Object monitor = new Object();
	public final static int TAM_CACHE = 10;
	private final static Map<String, Integer> cache = Collections.synchronizedMap(new HashMap<String, Integer>());


	public static void put(String cacheKey, Integer value) {
		if(cache.size() > TAM_CACHE) {
			cache.clear();
		}
		cache.put(cacheKey, value);
	}

	
	public static Object get(String cacheKey) {
		return cache.get(cacheKey);
	}
	
	public static void clear() {
		cache.clear();
	}

	
	public static long size() {
		return cache.size();
	}

	
	public static void list() {
		cache.forEach((k,v) -> System.out.println(v));
	}
	
	public static SimpleCacheManager getInstance() {
		if (instance == null) {
			synchronized (monitor) {
				if (instance == null) {
					instance = new SimpleCacheManager();
				}
			}
		}
		return instance;
	}

}
