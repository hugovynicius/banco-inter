package banco.inter.exercise.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import banco.inter.exercise.model.Digito;

@Repository
public interface DigitoRepository extends JpaRepository<Digito, Long>{
	
	@Query("SELECT d FROM Digito d JOIN FETCH d.usuario u WHERE u.id = :idUsuario")
	public List<Digito> buscarDigitosUsuario(@Param("idUsuario")Long idUsuario);
	
}
