package banco.inter.exercise.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import banco.inter.exercise.model.Usuario;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
	
	@Override
	@Query("SELECT u FROM Usuario u JOIN FETCH u.digitos d ")
	public List<Usuario> findAll();
}
