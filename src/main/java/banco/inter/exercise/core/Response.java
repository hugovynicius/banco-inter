package banco.inter.exercise.core;

import java.util.ArrayList;
import java.util.List;

public class Response<T> {

	private T data;
	private List<T> listData;
	private List<String> errors;

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
	
	public void setData(List<T> lista) {
		this.listData = lista;
	}
	
	public List<T> getListData() {
		return listData;
	}

	public void setListData(List<T> listData) {
		this.listData = listData;
	}

	public List<String> getErrors() {
		if (this.errors == null) {
			this.errors = new ArrayList<String>();
		}
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

	public Response() {
	}
}
