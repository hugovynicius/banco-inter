package banco.inter.exercise.core;

import java.util.ArrayList;
import java.util.List;

import banco.inter.exercise.dto.FormDigito;
import banco.inter.exercise.model.Digito;

public class Conversor {
	
	public List<FormDigito> conversor(List<Digito> digitos){
		List<FormDigito>  listForm = new ArrayList<>();
		if(digitos != null) {
			digitos.forEach(d -> {
				FormDigito form = new FormDigito();
				form.setN(d.getParametroN().toString());
				form.setK(d.getParametroK());
				form.setResultado(d.getResultado());
				listForm.add(form);
			});
		}
		return listForm;	
	}
}
