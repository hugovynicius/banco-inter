package banco.inter.exercise.core;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@SuperBuilder
public class ResponseBase<T> {
	private T dados;
	private List<String> erros;
	
	public ResponseBase(T dados){
		this.dados = dados;
		this.erros = new ArrayList<>();
	}
	
	public ResponseBase(List<String> erros){
		this.dados = null;
		this.erros = erros;		
	}
}
