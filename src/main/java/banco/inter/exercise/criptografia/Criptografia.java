package banco.inter.exercise.criptografia;

import java.security.PrivateKey;
import java.security.PublicKey;

public interface Criptografia {
	
	void geraChave();
	
	boolean verificaSeExisteChavesNoSO();
	
	byte[] criptografa(String texto, PublicKey chave);
	
	String decriptografa(byte[] texto, PrivateKey chave);
	
	
}
