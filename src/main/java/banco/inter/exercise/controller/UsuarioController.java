package banco.inter.exercise.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import banco.inter.exercise.dto.FormDigito;
import banco.inter.exercise.dto.FormUsuario;
import banco.inter.exercise.model.Usuario;
import banco.inter.exercise.service.UsuarioService;

@RestController
@RequestMapping("/api/usuario")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;

	
	@GetMapping("/inter/")
    public ResponseEntity<String> get(){
        return new ResponseEntity<>("APP OK", HttpStatus.OK);
    }
	
	@GetMapping
	public ResponseEntity<List<FormUsuario>> getAll() {	
		return ResponseEntity.ok().body(usuarioService.getAll());
	}

	@GetMapping("/{id}")
	public ResponseEntity<Usuario> get(@PathVariable("id") Long id) {
		return ResponseEntity.ok().body(usuarioService.get(id));
	}
	
	@PostMapping(path="/salvar/")
	public ResponseEntity<String> salvar(@RequestBody FormUsuario formUsuario) {
		return ResponseEntity.ok().body(this.usuarioService.save(formUsuario));		
	}
	
	@GetMapping(path = "/digito/{id}")
	public ResponseEntity<List<FormDigito>> buscarDigitos(@PathVariable("id") Long id) {
		return ResponseEntity.ok().body(usuarioService.getDigitosUsuario(id));
	}
	
	
	@PostMapping(path ="/calcula/")
    public ResponseEntity<Integer> calculaDigitos(@RequestBody FormDigito formDigito){
		return ResponseEntity.ok().body(usuarioService.calculoDigito(formDigito));
    }
	
	@GetMapping(path ="/chave/")
    public ResponseEntity<String> getChave() throws FileNotFoundException, ClassNotFoundException, IOException{
		return ResponseEntity.ok().body(usuarioService.getChave());
    }
	
}
