package banco.inter.exercise.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import banco.inter.exercise.dto.FormDigito;
import banco.inter.exercise.dto.FormUsuario;
import banco.inter.exercise.model.Digito;
import banco.inter.exercise.model.Usuario;

public interface UsuarioService {

	Usuario get(Long id);

	List<FormUsuario> getAll();

	String save(FormUsuario usuario);

	void delete(Long id);
	
	List<FormDigito> getDigitosUsuario(Long id);
	
	Integer calculoDigito(FormDigito formFigito);
	
	String getChave() throws FileNotFoundException, ClassNotFoundException, IOException;

}
