package banco.inter.exercise.service.impl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import banco.inter.exercise.cache.Cache;
import banco.inter.exercise.calc.DigitoUnico;
import banco.inter.exercise.core.Conversor;
import banco.inter.exercise.criptografia.CriptografiaRSA;
import banco.inter.exercise.dto.FormDigito;
import banco.inter.exercise.dto.FormUsuario;
import banco.inter.exercise.model.Digito;
import banco.inter.exercise.model.Usuario;
import banco.inter.exercise.repository.DigitoRepository;
import banco.inter.exercise.repository.UsuarioRepository;
import banco.inter.exercise.service.UsuarioService;
//import lombok.extern.slf4j.Slf4j;

//@Slf4j
@Service
public class UsuarioServiceImpl implements UsuarioService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UsuarioServiceImpl.class);
	
	private static int MIN = 1;
	private static int MAX = 10000;
	private static int MAX_5 = 4;
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private DigitoRepository digitoRepository;
		
	@Override
	public Usuario get(Long id) {
		Usuario usuario = usuarioRepository.findById(id).orElseThrow(() -> new NoSuchElementException("Usuario not found."));
		return usuario;
	}

	@Override
	public List<FormUsuario> getAll() {
		List<Usuario> usuarioBrutos = usuarioRepository.findAll();
		List<FormUsuario> usuarioDesc = new ArrayList<FormUsuario>();
		Conversor conv = new Conversor();
		
		for(Usuario u : usuarioBrutos) {
			try {
				FormUsuario usr = new FormUsuario();
				usr.setId(u.getId());
				usr.setNome(this.retornaTextoPuro(u.getNome()));
				usr.setEmail(this.retornaTextoPuro(u.getEmail()));
				usr.setDigitos(conv.conversor(u.getDigitos()));
				usuarioDesc.add(usr);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		return usuarioDesc;
	}
		
	@Override
	public String save(FormUsuario formUsuario) {
		
		Cache.getInstance();
		
		try {
			CriptografiaRSA criptografia = new CriptografiaRSA();
			Usuario usuario = new Usuario();
			
			try {
				PublicKey chavePublica = this.retornaChavePublica();
				//Nome criptografado 
				usuario.setNome(criptografia.criptografa(formUsuario.getNome(),chavePublica));
				//Email criptografado
				usuario.setEmail(criptografia.criptografa(formUsuario.getEmail(),chavePublica));
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			Usuario usuarioSave = usuarioRepository.saveAndFlush(usuario);
			
			//Geração do digito único 
			Integer n	= (int) ((Math.random() * ((MAX- MIN) + 1)) + MIN);
			Integer k	= (int) ((Math.random() * ((MAX_5- MIN) + 1)) + MIN);
			
			Digito dig = new Digito();
			Integer calcDig = (Integer) Cache.get(n.toString()+k.toString());
			
			//verifica se foi gerado um dígito único
			if(calcDig == null) {	
				DigitoUnico digito = new DigitoUnico(String.valueOf(n),k);
				dig.setResultado(digito.getResultado());
				dig.setParametroN(digito.getParametros().getN());
				dig.setParametroK(digito.getParametros().getK());
				
				//atribui o resultado ao cache 
				Cache.put(dig.getParametroN().toString()+dig.getParametroK().toString(), dig.getResultado());
				dig.setUsuario(usuarioSave);
				digitoRepository.save(dig);
			}else {
				dig.setResultado(calcDig);
				dig.setParametroN(n);
				dig.setParametroK(k);
				dig.setUsuario(usuarioSave);
				digitoRepository.save(dig);
			}
			
			LOGGER.debug("Sucesso ao salvar usuário");
			
			return (new String("Sucesso ao salvar usuário"));
		} catch (Exception e) {
			LOGGER.debug("Erro ao salvar usuário - " + e.getMessage());
			return(new String("Erro ao salvar usuário - " + e.getMessage()));
		}
		
	}
	
	@Override
	public void delete(Long id) {
		usuarioRepository.deleteById(id);
	}

	@Override
	public List<FormDigito> getDigitosUsuario(Long id) {
		List<Digito> digitos = digitoRepository.buscarDigitosUsuario(id);
		Conversor conv = new Conversor();
		return conv.conversor(digitos);
	}

	@Override
	public Integer calculoDigito(FormDigito formDigito) {
		DigitoUnico digito = new DigitoUnico(formDigito.getN(),formDigito.getK());
		return digito.getResultado();
	}
	
	
	@SuppressWarnings("resource")
	public PublicKey retornaChavePublica() throws FileNotFoundException, IOException, ClassNotFoundException {
		CriptografiaRSA criptografia = new CriptografiaRSA();
		
		if (!criptografia.verificaSeExisteChavesNoSO()) {
			criptografia.geraChave();
		}

		ObjectInputStream inputStream = null;
		inputStream = new ObjectInputStream(new FileInputStream(CriptografiaRSA.PATH_CHAVE_PUBLICA));
		final PublicKey chavePublica = (PublicKey) inputStream.readObject();
		return (chavePublica);
	}
	
	@SuppressWarnings("resource")
	public String retornaTextoPuro(byte[] textoCriptografado) throws ClassNotFoundException, IOException {
		try {
			CriptografiaRSA criptografia = new CriptografiaRSA();
			ObjectInputStream inputStream = null;
			inputStream = new ObjectInputStream(new FileInputStream(CriptografiaRSA.PATH_CHAVE_PRIVADA));
			PrivateKey chavePrivada = (PrivateKey) inputStream.readObject();
			String textoPuro = criptografia.decriptografa(textoCriptografado, chavePrivada);
			LOGGER.debug("Sucesso em retornar o texto puro");
			return textoPuro;
		} catch (Exception e) {
			LOGGER.debug("Erro em retornar o texto puro");
			return new String("");
		}
		
	}
	
	@Override
	public String getChave() throws FileNotFoundException, ClassNotFoundException, IOException {
		String chave = new String(this.retornaChavePublica().toString());
		return(chave);
	}
	
	

}
