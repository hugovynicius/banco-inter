package banco.inter.exercise.calc;

public class Parametros {
	
	private Integer n;
	private Integer k;
	private String p;
	
	public Parametros(Integer n, Integer k) {
		this.n = n;
		this.k = k;
		this.p = this.calculaP();
	}
	
	public String getP() {
		return p;
	}
	
	public Integer getN() {
		return n;
	}

	public Integer getK() {
		return k;
	}

	private String calculaP() {
		String p1 = String.valueOf(this.n);
		String p = "";
		
		for(int i = 0; i < this.k; i++) {
			p = p + p1;
		}
		return(p);
	}	
	
}
