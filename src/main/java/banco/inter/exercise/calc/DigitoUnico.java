package banco.inter.exercise.calc;

public class DigitoUnico {
	
	private Integer Resultado;
	private Parametros parametros;
	
	public DigitoUnico(String n, Integer k) {
		this.parametros =  new Parametros(Integer.valueOf(n),k);
		this.Resultado = this.processaDigitoUnico();
	}
	
	public Integer getResultado() {
		return Resultado;
	}

	public Parametros getParametros() {
		return parametros;
	}
	
	//Retorno inteiro 
	private Integer digitoUnico(Long numero) {
		if(String.valueOf(numero).length() == 1) {
			return(numero.intValue());
		}else {
			return(digitoUnico(Long.valueOf(this.somarDigitos(String.valueOf(numero)))));
		}	
	}
	
	private Integer somarDigitos(String digitos) {
		Integer soma = Integer.valueOf(0);
		Integer tam  = String.valueOf(digitos).length();
		for(int i = 0; i < tam; i++) {
			soma = soma.intValue() +  Integer.parseInt(String.valueOf(digitos.charAt(i)));
		}
		return (soma);
	}
	
	private Integer processaDigitoUnico() {
		Integer resultado = this.digitoUnico(Long.valueOf(this.parametros.getP()));
		return(resultado);
	}
	
}
